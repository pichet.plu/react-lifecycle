import React, { Component } from 'react';

class Foo extends Component {
  componentDidMount() {
    console.log('Foo componentDidMount');
  }
  componentWillReceiveProps(nextProps) {
    console.log('Foo componentWillReceiveProps', nextProps);
  }
  componentWillUpdate(nextProps, nextState) {
    console.log('Foo componentWillUpdate', nextProps);
  }
  componentWillUnmount() {
    console.log('Foo componentWillUnmount');
  }
  render() {
    return <p>Foo!</p>
  }
}
class Bar extends Component {
  componentDidMount() {
    console.log('Bar componentDidMount');
  }
  componentWillReceiveProps(nextProps) {
    console.log('Bar componentWillReceiveProps', nextProps);
  }
  componentWillUpdate(nextProps, nextState) {
    console.log('Bar componentWillUpdate', nextProps);
  }
  componentWillUnmount() {
    console.log('Bar componentWillUnmount');
  }
  render() {
    return <p>Bar!</p>
  }
}
class App extends Component {
  state = {
    div: true
  }
  componentDidMount() {
    console.log('App componentDidMount');
  }
  componentWillReceiveProps(nextProps) {
    console.log('App componentWillReceiveProps', nextProps);
  }
  componentWillUpdate(nextProps, nextState) {
    console.log('App componentWillUpdate', nextProps);
  }
  componentWillUnmount() {
    console.log('App componentWillUnmount');
  }
  handleClick = () => {
    this.setState(prevState => ({ div: !prevState.div }))
  }
  getComponent = () => {
    const foo = <Foo a={1} b={2} c={3} />

    if (this.state.div) {
      return (
        <div>{foo}</div>
      )
    }
    else {
      return (
        <span>{foo}</span>
      )
    }
  }
  render(){
    return(
      <div>
        <button onClick={this.handleClick}>Click</button>
        {this.getComponent()}
        <Bar {...this.state} />
      </div>
    )
  }

}

export default App;
